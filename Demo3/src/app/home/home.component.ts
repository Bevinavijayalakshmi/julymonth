import { Component, OnInit } from '@angular/core';
import { ServiceboxService } from './../servicebox.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

todaydate;
constructor(private Servicebox: ServiceboxService) {}

  ngOnInit(): void {
    this.todaydate = this.Servicebox.showDate();
  }

}
