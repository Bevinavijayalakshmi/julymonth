import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ServiceboxService {
showDate(){
    let mydate= new Date();
    return mydate;
  }
  private apiurl = "http://jsonplaceholder.typicode.com/users";
   constructor(private http: HttpClient) { }
   getData() {
      return this.http.get(this.apiurl);
   }
}
