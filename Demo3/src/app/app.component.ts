import { Component } from '@angular/core';
import { ServiceboxService } from './servicebox.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FirstDemo';
  department=["ECE","CSE","EEE"];
  isAvailable=true;
  myclick(event){
    alert("button is clicked");
    console.log(event);
  }
  public persondata = [];
 constructor(private myservice: ServiceboxService) {}
 ngOnInit() {
    this.myservice.getData().subscribe((data) => {
       this.persondata = Array.from(Object.keys(data), k=>data[k]);
       console.log(this.persondata);
    });
 }
  }











