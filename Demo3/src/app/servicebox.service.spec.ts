import { TestBed } from '@angular/core/testing';

import { ServiceboxService } from './servicebox.service';

describe('ServiceboxService', () => {
  let service: ServiceboxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceboxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
